# Bacon Bot

### Features

* Ping-pong
* Whois lookup
* Ban spammers on join
* Set nickname on join
* Delete naughty text and warn user
* Delete embed/attachment spam and warn user

### Permissions

The following permissions are required to perform certain actions:

* Kick Members - to kick spammers on arrival
* Ban Members - to ban members (might not be necessary)
* Manage Nicknames - to change effective name on join
* Manage Emojis - to remove inappropriate reactions emojis
* Manage Messages - to remove inappropriate messages

### Word Filters

All posted messages are checked against a set of word filters. For information on how to edit the filters, read the
instructions in the [resources directory](src/main/resources).

### Batch nickname change

To launch the application on a local computer, first download the latest job artifact (left pane menu, `CI / CD` >
`Jobs`, cloud download symbol) from GitLab. Extract the downloaded zip, and run the included jar file, `bacon-bot.jar`
from the command line:

```
java -jar bacon-bot.jar DISCORD_API_TOKEN
```

(Substitute `DISCORD_API_TOKEN` with the actual API token.)

By default, the application will perform a "dry run". When you are sure you want to actually replace the listed
nicknames, run the application with the `-x/--execute` option:

```
java -jar bacon-bot.jar -x DISCORD_API_TOKEN
```

For convenience, a help text is available when running the application with the `-h/--help` option:

```
java -jar bacon-bot.jar -h
```
