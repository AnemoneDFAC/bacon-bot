# Banned text

The filters are parsed as tab separated CSV files, using words and phrases from the first column. The second column
can be used for comments and descriptions, to provide context for maintaining the filters.

If using an IDE to edit the csv files, make sure that tabs are not converted to 4 spaces. In IntelliJ IDEA, go to
"Settings" > "Code Style" > "Other File Types", and make sure "Use tab character" has been checked.

## Word filters

Auto-moderating of messages is based on two separate lists of banned text:

### Exact match

Message contains a word which is exactly as defined in [banned_text_exact_match.csv](banned_text_exact_match.csv)

### Any match

Message contains a word which has a substring as defined in [banned_text_any_match.csv](banned_text_any_match.csv)

## Updating filters

The filters can be updated by adding to either of the csv-files. When making changes, it is a good idea to add examples
files in the [test resources directory](../../test/resources). These files are used for automated tests, and is an extra
precaution to make sure we don't deploy changes which don't work as intended.

### Make some changes

To start making some changes to the filters, select either of the csv-files and click "Edit". When you're happy with
your changes, add a descriptive commit message in the text input at the bottom, come up with a new name for your target
branch (cannot contain spaces), and press "Commit changes". Make sure "Start a new merge request with these changes" is
checked.

On the next page, press "Submit merge request". You can still add changes, but creating the merge request at this stage
saves you from having to come back to this page later.

If you have more to add, select "Repository" > "Branches" from the left-hand menu, and click on your newly
created branch from the list of active branches. Keep adding changes and clicking "Commit changes" at the bottom until
you're happy.

To review your changes when you're done, select "Merge Requests" from the left-hand menu, and click on your merge
request from the list of open merge requests. It will be named after your first commit message, so make sure you made a
descriptive commit message when you started making changes.