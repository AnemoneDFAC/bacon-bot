package extension

import config.Filter
import config.Filter.nicknameReplacementCharacters

fun String.containsBannedText() =
    Filter.bannedTextExactMatch.containsMatch(this) or Filter.bannedTextAnyMatch.containsMatch(this)

fun String.withReplacedCharacters(): String {
    var nickname = ""
    var replaced = false

    for (char in this.reversed()) {
        if (!replaced && nicknameReplacementCharacters.containsKey(char)) {
            nickname += nicknameReplacementCharacters[char]
            replaced = true
        } else {
            nickname += char
        }
    }
    if (!replaced) {
        return nickname.reversed() + "."
    }
    return nickname.reversed()
}
