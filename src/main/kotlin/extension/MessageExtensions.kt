package extension

import config.Command
import net.dv8tion.jda.core.entities.Message

fun Message.parse(): Pair<Command?, String> {
    return when {
        this.contentRaw.startsWith("!ping") -> {
            val content = this.contentRaw.substring(5)
            Command.PING to content
        }
        this.contentRaw.startsWith("!whois") -> {
            val content = this.contentRaw.substring(6)
            Command.WHOIS to content.trim()
        }
        else -> null to this.contentRaw
    }
}
