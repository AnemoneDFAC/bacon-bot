package extension

import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.TextChannel

fun Guild.getTextChannelByType(type: config.Guild.Channel): TextChannel? {
    val channelId = config.Guild.getChannel(this.idLong, type) ?: return null
    return this.jda.getTextChannelById(channelId)
}
