package filter

import java.util.Objects
import java.util.concurrent.Delayed
import java.util.concurrent.TimeUnit
import java.time.Instant


class EmbedDelayObject(val userId: String) : Delayed {
    private val expiry: Long = Instant.now().plusSeconds(EXPIRY_IN_SECONDS).toEpochMilli()

    companion object {
        const val EXPIRY_IN_SECONDS = 300L
    }

    override fun getDelay(timeUnit: TimeUnit): Long {
        val diff = expiry - System.currentTimeMillis()
        return timeUnit.convert(diff, TimeUnit.MILLISECONDS)
    }

    override fun compareTo(other: Delayed): Int {
        return java.lang.Long.compare(this.expiry, (other as EmbedDelayObject).expiry)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as EmbedDelayObject?
        return userId == that!!.userId
    }

    override fun hashCode(): Int {
        return Objects.hash(userId)
    }
}