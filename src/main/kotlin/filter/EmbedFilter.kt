package filter

import net.dv8tion.jda.core.entities.Message
import extension.toHumanReadable
import net.dv8tion.jda.core.entities.User
import java.time.Duration
import java.time.Instant
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.DelayQueue

private val embedDelayQueue = DelayQueue<EmbedDelayObject>()
private val embedTimestamp = ConcurrentHashMap<String, Instant>()

fun isSpammingEmbeds(message: Message): Boolean {
    if (message.embeds.isEmpty() && message.attachments.isEmpty()) {
        return false
    }
    return isOnCooldown(message.member.user.id)
}

fun isOnCooldown(id: String): Boolean {
    removeFromQueueIfExpired()
    if (embedDelayQueue.contains(EmbedDelayObject(id))) {
        return true
    }
    addToQueue(id)
    return false
}

private fun addToQueue(userId: String) {
    embedDelayQueue.add(EmbedDelayObject(userId))
    embedTimestamp[userId] = Instant.now()
}

private fun removeFromQueueIfExpired() {
    var delayEmbed = embedDelayQueue.poll()
    while (delayEmbed != null) {
        embedTimestamp.remove(delayEmbed.userId)
        delayEmbed = embedDelayQueue.poll()
    }
}

fun User.getRemainingCooldown(): String? {
    val cooldownStart: Instant = embedTimestamp[this.id] ?: return null
    val cooldownClear: Instant = cooldownStart.plusSeconds(EmbedDelayObject.EXPIRY_IN_SECONDS)
    val remainingCooldown = Duration.between(Instant.now(), cooldownClear)

    return remainingCooldown.toHumanReadable()
}
