import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.mainBody
import net.dv8tion.jda.core.JDABuilder
import net.dv8tion.jda.core.OnlineStatus
import java.time.Instant

val applicationStart: Instant = Instant.now()
var executeNicknameReplacements = false

class ParsedArgs(parser: ArgParser) {
    val execute by parser.flagging(
        "-x", "--execute",
        help = "Execute nickname replacement")
    val token by parser.positional(
        "TOKEN",
        help = "Discord API token")
}

fun main(args: Array<String>) = mainBody<Unit> {
    ArgParser(args).parseInto(::ParsedArgs).run {
        executeNicknameReplacements = execute

        JDABuilder(token)
            .setStatus(OnlineStatus.ONLINE)
            .addEventListener(CommandListener())
            .build()
            .awaitReady()
    }
}
