package config.dto

import config.Guild

data class GuildDto(
    val name: String,
    val id: Long,
    val channels: Map<Guild.Channel, Long>?,
    val `admin roles`: List<String>?
)
