package config.dto

data class WarningDto(
    val `naughty text warning`: String,
    val `naughty emote warning`: String,
    val `embed spam warning`: String,
    val `kick member reason`: String,
    val `kick member message`: String
)
