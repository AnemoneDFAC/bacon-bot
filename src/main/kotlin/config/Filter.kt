package config

import com.fasterxml.jackson.core.type.TypeReference
import com.opencsv.CSVParserBuilder
import com.opencsv.CSVReaderBuilder
import config.dto.FilterDto
import config.serialize.BaseConfig
import org.ahocorasick.trie.Trie

object Filter: BaseConfig<FilterDto>("filters.yml", object: TypeReference<FilterDto>() {}) {

    private const val BANNED_TEXT_ANY_MATCH_FILE = "banned_text_any_match.csv"
    private const val BANNED_TEXT_EXACT_MATCH_FILE = "banned_text_exact_match.csv"

    val bannedTextAnyMatch = buildBannedTextAnyMatchTrie()
    val bannedTextExactMatch = buildBannedTextExactMatchTrie()
    val nicknameReplacementCharacters = config.`nickname replacement characters`

    private fun buildBannedTextAnyMatchTrie(): Trie {
        val anyMatchBuilder = Trie.builder().ignoreCase()
        for (line in readTextFromCsv(BANNED_TEXT_ANY_MATCH_FILE)) {
            anyMatchBuilder.addKeyword(line)
        }
        return anyMatchBuilder.build()
    }

    private fun buildBannedTextExactMatchTrie(): Trie {
        val exactMatchBuilder = Trie.builder().ignoreCase().onlyWholeWords()
        for (line in readTextFromCsv(BANNED_TEXT_EXACT_MATCH_FILE)) {
            exactMatchBuilder.addKeyword(line)
        }
        return exactMatchBuilder.build()
    }

    private fun readTextFromCsv(fileName: String): List<String> {
        val inputStream = Filter.javaClass.classLoader.getResourceAsStream(fileName)
        val csvParser = CSVParserBuilder().withSeparator('\t').withIgnoreQuotations(true).build()
        val csvReader = CSVReaderBuilder(inputStream.bufferedReader()).withCSVParser(csvParser).build()
        val lines = csvReader.readAll().mapNotNull { it[0] }.filter { it.isNotBlank() }
        csvReader.close()
        return lines
    }
}
