import com.fleskesvor.BuildConfig
import config.Filter.nicknameReplacementCharacters
import extension.isAdmin
import mu.KotlinLogging
import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.events.ReadyEvent
import net.dv8tion.jda.core.exceptions.RateLimitedException
import net.dv8tion.jda.core.hooks.ListenerAdapter
import java.lang.Thread.sleep

private val logger = KotlinLogging.logger {}
private const val DFAF = 357444624178216961

class CommandListener: ListenerAdapter() {

    override fun onReady(event: ReadyEvent) {
        logger.info { "API version ${BuildConfig.VERSION} is ready on ${BuildConfig.ENVIRONMENT}" }

        val guild = event.jda.getGuildById(DFAF)
        for (member in guild.members.sortedBy { member -> member.effectiveName.toLowerCase() }) {

            if (!member.isAdmin() && member.nickname == null) {
                val nickname = createNickname(member.user.name)

                if (member.user.name != nickname) {
                    if (executeNicknameReplacements) {
                        executeNicknameChange(guild, member, nickname)
                    } else {
                        printNicknameChange(member, nickname)
                    }
                }
            }
        }
    }

    private fun printNicknameChange(member: Member, nickname: String) {
        println("${member.user.id}\t${member.user.name}\t$nickname")
    }

    private fun executeNicknameChange(guild: Guild, member: Member, nickname: String) {
        try {
            guild.controller.setNickname(member, nickname).complete()
        } catch (e: RateLimitedException) {
            sleep(e.retryAfter)
            guild.controller.setNickname(member, nickname).complete()
        }
    }

    private fun createNickname(name: String): String {
        var nickname = ""
        var replaced = false

        for (char in name.reversed()) {
            if (! replaced && nicknameReplacementCharacters.containsKey(char)) {
                nickname += nicknameReplacementCharacters[char]
                replaced = true
            } else {
                nickname += char
            }
        }
        if (!replaced) {
            return nickname.reversed() + "."
        }
        return nickname.reversed()
    }
}
