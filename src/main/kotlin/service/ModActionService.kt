package service

import config.Warning
import extension.getTextChannelByType
import extension.isAdmin
import extension.withReplacedCharacters
import filter.getRemainingCooldown
import mu.KotlinLogging
import net.dv8tion.jda.core.entities.*

private val logger = KotlinLogging.logger {}

// Signatures of higher-order functions to be invoked on success callbacks
private typealias modAction = (reason: String, member: Member) -> Unit

fun deleteMessageWarnMember(warning: String, message: Message, member: Member) {
    if (member.isAdmin()) return
    message.delete().queue() // FIXME: Log deletion
    sendPrivateMessage(warning, member)
}

fun deleteReactionWarnMember(warning: String, reaction: MessageReaction, member: Member) {
    if (member.isAdmin()) return
    reaction.removeReaction().queue() // FIXME: This ain't working
    sendPrivateMessage(warning, member)
}

fun deleteEmbedWarnMember(warning: String, message: Message, member: Member) {
    if (member.isAdmin()) return
    val remainingCooldown = member.user.getRemainingCooldown()
    message.delete().queue() // FIXME: Log deletion
    sendPrivateMessage(warning.format(remainingCooldown), member)
}

fun kickMemberLogAction(member: Member, guild: Guild) {
    val reason = Warning.kickMemberReason.format(member.effectiveName)
    val message = Warning.kickMemberMessage.format(guild.name)
    sendPrivateMessage(message, member, reason, kickMember, logModeratorAction)
}

fun assignNickname(member: Member, guild: Guild) {
    val nickname = member.effectiveName.withReplacedCharacters()
    logModeratorAction("Assigning nickname $nickname to member ${member.effectiveName}", member)

    guild.controller.setNickname(member, nickname).queue()
    postWelcomeMessage(member, guild)
}

// Takes a variable number of Functions that must be called after message has been sent
private fun sendPrivateMessage(message: String, member: Member,
                               reason: String? = null, vararg callbacks: modAction) {
    member.user.openPrivateChannel().queue {
        it.sendMessage(message).queue {
            // TODO: Should functions be called even if message was not sent?
            callbacks.forEach { it(reason!!, member) }
        }
    }
}

private fun postWelcomeMessage(member: Member, guild: Guild) {
    val welcomeChannel = guild.getTextChannelByType(config.Guild.Channel.WELCOME)
    val welcomeMessage = config.Message.welcomeMessage(guild.idLong, member.asMention)
    welcomeChannel?.sendMessage(welcomeMessage)?.queue {
        logger.info { "Sent message: ${it.jumpUrl}" }
    }
}

private val logModeratorAction: modAction = { message, member ->
    val logChannel = member.guild.getTextChannelByType(config.Guild.Channel.LOGS)
    logChannel?.sendMessage(message)?.queue()
    logger.info { message }
}

private val kickMember: modAction = { reason, member ->
    // Kick reason is recorded in the audit log
    member.guild.controller.kick(member, reason).queue()
}
